From refinedc.typing Require Import typing.
From refinedc.tutorial.t03_list Require Import generated_code.
From refinedc.tutorial.t03_list Require Import generated_spec.
From caesium Require Import builtins_specs.
Set Default Proof Using "Type".

(* Generated from [tutorial/t03_list.c]. *)
Section proof_rev_append.
  Context `{!typeG Σ} `{!globalG Σ}.

  (* Typing proof for [rev_append]. *)
  Lemma type_rev_append :
    ⊢ typed_function impl_rev_append type_of_rev_append.
  Proof.
    Local Open Scope printing_sugar.
    start_function "rev_append" ([[[v p] l1] l2]) => arg_l1 arg_l2 local_cur local_cur_tail.
    prepare_parameters (v p l1 l2).
    split_blocks ((
      <[ "#1" :=
        ∃ v_suffix : val,
        ∃ l_suffix : list type,
        ∃ l_prefix : list type,
        arg_l1 ◁ₗ (at_value (v) (l1 @ (list_t))) ∗
        local_cur ◁ₗ (at_value (v_suffix) (l_suffix @ (list_t))) ∗
        local_cur_tail ◁ₗ (at_value (v_suffix) (l_suffix @ (list_t))) ∗
        arg_l2 ◁ₗ (p @ (&own (((rev l_prefix) ++ l2) @ (list_t))))
    ]> $
      ∅
    )%I : gmap label (iProp Σ)) ((
      ∅
    )%I : gmap label (iProp Σ)).
    - 

      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      (* loop invariant holds initially *)
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      (* stuck at this goal *)
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      liRStep; liShow.
      all: print_typesystem_goal "rev_append" "#0".
    - repeat liRStep; liShow.
      all: print_typesystem_goal "rev_append" "#1".
    Unshelve. all: li_unshelve_sidecond; sidecond_hook; prepare_sideconditions; normalize_and_simpl_goal; try solve_goal; unsolved_sidecond_hook.
    all: print_sidecondition_goal "rev_append".
    Unshelve. all: try done; try apply: inhabitant; print_remaining_shelved_goal "rev_append".
  Qed.
End proof_rev_append.

From refinedc.typing Require Import typing.
From refinedc.tutorial.t03_list Require Import generated_code.
From refinedc.tutorial.t03_list Require Import generated_spec.
From caesium Require Import builtins_specs.
Set Default Proof Using "Type".

(* Generated from [tutorial/t03_list.c]. *)
Section proof_member.
  Context `{!typeG Σ} `{!globalG Σ}.

  (* Typing proof for [member]. *)
  Lemma type_member :
    ⊢ typed_function impl_member type_of_member.
  Proof.
    Local Open Scope printing_sugar.
    start_function "member" ([[l q] n]) => arg_p arg_k local_prev local_cur local_head.
    prepare_parameters (l q n).
    split_blocks ((
      <[ "#1" :=
        ∃ pr : loc,
        ∃ l_suffix : list Z,
        arg_k ◁ₗ (n @ (int (size_t))) ∗
        local_cur ◁ₗ uninit void* ∗
        local_head ◁ₗ uninit void* ∗
        local_prev ◁ₗ (pr @ (&own ((l_suffix `at_type` int size_t) @ (list_t)))) ∗
        arg_p ◁ₗ (q @ (&own (wand (pr ◁ₗ (l_suffix `at_type` int size_t) @ list_t) ((l `at_type` int size_t) @ (list_t))))) ∗
        ⌜ exists l_prefix, (and (l = l_prefix ++ l_suffix) (n ∉ l_prefix)) ⌝
    ]> $
      ∅
    )%I : gmap label (iProp Σ)) ((
      ∅
    )%I : gmap label (iProp Σ)).
    - repeat liRStep; liShow.
      all: print_typesystem_goal "member" "#0".
    - repeat liRStep; liShow.
      all: print_typesystem_goal "member" "#1".
    Unshelve. all: li_unshelve_sidecond; sidecond_hook; prepare_sideconditions; normalize_and_simpl_goal; try solve_goal; unsolved_sidecond_hook.
    +  exists []; set_unfold; refined_solver. 
    +  set_unfold; refined_solver. 
    +  rewrite cons_middle app_assoc; eexists; split; first done; set_unfold; refined_solver. 
    all: print_sidecondition_goal "member".
    Unshelve. all: try done; try apply: inhabitant; print_remaining_shelved_goal "member".
  Qed.
End proof_member.

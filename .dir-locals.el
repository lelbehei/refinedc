((nil . ((eval . (setq build-file-name "build.sh"))
         (eval . (defun build-cmd ()
                   "Search in the current and parent directories for a file with the name of variable `build-file-name' and execute the first file it find."
                   (interactive)
                   (save-some-buffers t) ; save all buffers
                   (let ((buildfile (locate-dominating-file default-directory build-file-name)))
                     (unless buildfile (error "No build file found!"))
                     (let ((default-directory buildfile))
                       (compile (concat buildfile build-file-name " " (buffer-file-name))))
                     (select-window (get-buffer-window "*compilation*"))))) 
         (eval . (setq projectile-project-compilation-cmd #'build-cmd))
         )
      ))
